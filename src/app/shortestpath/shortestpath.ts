import {Component, ViewChild, OnInit } from '@angular/core';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subject, merge} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import { DataService } from '../app.dataservice';

const sourceplanet = ['Earth','Moon','Jupiter','Venus','Mars','Saturn','Uranus','Pluto','Neptune','Mercury','Alpha Centauri','Luhman 16','Epsilon Eridani','Groombridge 34','Epsilon Indi','Tau Ceti','Kapteyns star','Gliese 687','Gliese 674','Gliese 876#','Gliese 832','Fomalhaut','Virginis','HD 102365','Gliese 176','Gliese 436','Pollux','Gliese 86','HIP 57050','Piscium','GJ 1214','Upsilon Andromedae','Gamma Cephei','HD 176051','Gliese 317','HD 38858','Ursae Majoris'];

const destplanet = ['Earth','Moon','Jupiter','Venus','Mars','Saturn','Uranus','Pluto','Neptune','Mercury','Alpha Centauri','Luhman 16','Epsilon Eridani','Groombridge 34','Epsilon Indi','Tau Ceti','Kapteyns star','Gliese 687','Gliese 674','Gliese 876#','Gliese 832','Fomalhaut','Virginis','HD 102365','Gliese 176','Gliese 436','Pollux','Gliese 86','HIP 57050','Piscium','GJ 1214','Upsilon Andromedae','Gamma Cephei','HD 176051','Gliese 317','HD 38858','Ursae Majoris'];

  interface Planet {
    Source: {
      Name: string,
      Image: string
    },
    Destination: {
      Name: string,
      Image: string
    }
    ShortestDistance: string;
    ShortestDistanceWithTraffic: string;
  }
  
  const PLANETS: Planet[] = [
    {
      Source: {
        Name: 'Mars',
        Image: '0/02/OSIRIS_Mars_true_color.jpg'
      },
      Destination: {
        Name: 'Venus',
        Image: 'e/e5/Venus-real_color.jpg'
      },
      ShortestDistance: '17075200',
      ShortestDistanceWithTraffic: '17075200'
    },
    
  ];

@Component({
  selector: 'app-shortestpath',
  templateUrl: './shortestpath.html',
  styleUrls: ['./shortestpath.css']
})
export class ShortestpathComponent implements OnInit {
  modelsource: any;
  modeldest: any;
  //planets = PLANETS;
  planets: any;
  planetsarr : Planet[];

  constructor(private dataService: DataService) {}
  
  ngOnInit() {

    
}

  @ViewChild('sourceinstance') sourceinstance: NgbTypeahead;
  sourceFocus$ = new Subject<string>();
  sourceClick$ = new Subject<string>();
  @ViewChild('destinstance') destinstance: NgbTypeahead;
  destFocus$ = new Subject<string>();
  destClick$ = new Subject<string>();

  searchsource = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const sourceClicksWithClosedPopup$ = this.sourceClick$.pipe(filter(() => !this.sourceinstance.isPopupOpen()));
    const inputFocus$ = this.sourceFocus$;

    return merge(debouncedText$, inputFocus$, sourceClicksWithClosedPopup$).pipe(
      map(term => (term === '' ? sourceplanet
        : sourceplanet.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  searchdest = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const destClicksWithClosedPopup$ = this.destClick$.pipe(filter(() => !this.destinstance.isPopupOpen()));
    const inputFocus$ = this.destFocus$;

    return merge(debouncedText$, inputFocus$, destClicksWithClosedPopup$).pipe(
      map(term => (term === '' ? destplanet
        : destplanet.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  findShortestDistance(){
    this.planets = PLANETS;
  //   this.dataService
  //       .getAll<any[]>()
  //       .subscribe((data: any[]) => this.planets = data,
  //       error => () => {
  //       },
  //       () => {
            
  //       });
   }
}
