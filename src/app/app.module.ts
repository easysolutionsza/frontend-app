
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { ShortestpathComponent } from './shortestpath/shortestpath';
import { NavComponent } from './nav/nav.component';
import {APP_BASE_HREF} from '@angular/common';
import { DataService } from './app.dataservice';
import { Configuration } from '../app/app.constants';

@NgModule({
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, HttpClientModule, NgbModule,AppRoutingModule],
  declarations: [AppComponent, ShortestpathComponent, NavComponent],
  bootstrap: [AppComponent],
  providers: [DataService, Configuration, HttpClientModule, {provide: APP_BASE_HREF, useValue : '/' }]
})
export class AppModule {}
