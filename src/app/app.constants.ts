import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public server = 'http://localhost:32827/api/';
    public apiUrl = 'shortestpath/sourceid=1,destid=2';
    public serverWithApiUrl = this.server + this.apiUrl;
}